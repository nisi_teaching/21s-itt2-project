---
layout: main
title: ITSL xml
permalink: /itsxml/
---

Its Learning compatible XML is generated based on the course plan.

To use the XML:

1. Download [the xml file]({{site.baseurl}}/assets/itslearning.xml)
1. Go to the relevant room
2. Go to plans
3. In the upper-right corner: `...` -> `Import/Export`
4. Click `Choose File` and select the xml file
5. Click `Import`. There will be an update of the text syaing that the XML has been imported
6. Click `Go back to plans`
7. Review and delete obsolete plans

No official guide to this was found. Some XML developper resources are [here](https://developer.itslearning.com/Create.Course.Planner.html).

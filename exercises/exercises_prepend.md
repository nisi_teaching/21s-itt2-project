---
title: 'COURSE TITLE'
subtitle: 'Exercises'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: 'COURSE TITLE, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans.

References

* [Weekly plans](https://EAL-ITT.gitlab.io/21s-itt2-project/21S_ITT2_PROJECT_weekly_plans.pdf)

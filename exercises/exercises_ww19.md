---
Week: 19
tags:
- website
- documentation
- CI/CD
- Signal processing
- Boxcar
- Filtering
- Mean
- Moving average
- Circular buffer
- Noise
---

# Exercises for ww19

## Exercise 1 - Project website

### Information

Your project is probably spread across multiple gitlab projects and the information in those gitlab projects might not be very clear for someone trying to find out the project objectives and the details of what and how you have built the solution.

In this exercise you will create a static website for the project using mkdocs, gitlab CI and gitlab pages.

The users of your website are companies where you are applying for internship and job. This means it has to be presentable and in a professional language. 


### Exercise instructions

1. To create the website follow the instructions from 1st semester [https://eal-itt.gitlab.io/discover-iot/cloud/gitlab-pages](https://eal-itt.gitlab.io/discover-iot/cloud/gitlab-pages)
2. The content of the website has to, as a minimum, include:
    * How you worked with project management and scrum - this has to include how you worked together with the OME's
    * How you organized your gitlab projects
    * Working with git and branching strategy
    * Your system's purpose and how it is used in the datacenter
    * Project plan
    * Pre mortem
    * System brainstorm
    * System overview (block diagram)
    * Use cases
    * Requirement draft
    * MQTT and how you are using it
    * OME education research
    * Measuring techniques research (week 7, exercise 1)
    * Sensor requirement draft
    * List of possible sensors (week 7, exercise 3)
    * Sensor data via. RPi and MQTT solution code (week 7, exercise 4)
    * Project Milestones
    * Data persistence using MongoDB exercise recreation steps and troubleshooting tips (week 9, exercise 1)
    * POC and MVP video's

3. Most of the content is stuff that you already have in your `readme.md`.  
    We would like to see a full presentation of the project organized in a way that makes sense to someone trying to understand how and what you have been doing, ie. do not make a subpage for each of the above.   Organizing it in categories and subpages is a better choice.  
    A navigation structure like the below is provided as an example, you can add or subtract to the needs of your project. 

    * Overview
        * Introduction (Text that briefly descibes the project)
        * Your system's purpose and how it is used in the datacenter
        * System overview
        * Screenshots from dashboard
    * Requirements
        * System brainstorm
        * Use cases
        * Requirement draft
    * Project management
        * How you worked with project management and scrum - this has to include how you worked together with the OME's
        * How you organized your gitlab projects
        * Working with git and branching strategy
        * How you did the project plan
        * How you did the pre mortem
        * OME education research
        * Project Milestones
    * Hardware
        * Measuring techniques research
        * Sensor requirement draft
        * List of possible sensors
    * Software
        * MQTT and how you are using it
        * Sensor data via. RPi and MQTT solution code
        * Data persistence using MongoDB
    * Testing
        * Planning of tests
        * MVP user test
        * Unit testing
        * System test
    * Documentation
        * Project plan
        * Pre mortem
        * POC and MVP video's
    * Ressources (links etc.)
        * Gitlab group
        * Course material
        * Articles
        * Official documentation

4. Place a link to your website at the top of your `readme.md` file
5. Notify the lecturers when you are ready for feedback on your completed website. 


## Exercise 2 - Signal processing

### Information

This is a solo exercise, feel free to do pair programming.

In this exercise, you will take a look at how you can process your sensor data so that it is more reliable and precise.  
In a project like yours, you will typically encounter outliers in your sensor data. These are typically due to noise in the environment the sensors are placed.  
This means that not only is the precision of the measurement affected by the precision of the sensor, but also the noise of the environment. To make it worse, noise is almost always random and cannot be removed in realtime.  
However, due to the random nature of noise, it can be reduced significantly.

### Exercise instructions

**Part 0 - Setting up the test**

Before you start working on processing your sensor data, it is a good idea to test your implementation on a known stream of data. This will make it easier to debug and tweak, since you know the expected result.

1. Create a python script called `SignalProcessing.py`
2. In `SignalProcesing.py`, create a function called `getNoisy` that takes in two floats: the *real value* and a *max noise*.
3. In `getNoisy`, make it so the function returns the real value + a random value between negative max noise and max noise.

You should now have a function which can generate noisy values from a known value.

**Part 1 - Creating a dynamic mean function**

One of the simplest ways to reduce the impact of noise is to take a series of measurements and find the mean value.  
The reason this reduces the noise is due to the random nature of the noise.  
The positive and negative fluctuations of the noise in the series of measurements helps cancel each other out, reducing the average noise.

1. In `SignalProcesing.py`, create a function called `getMean` that takes in a list of floats.
2. In `getMean`, make it so the function returns the mean of the floats in the list. This is done by summing up the values and dividing the sum with the number of values.

You should now have a function which can take in a list of floats and return the mean.

**Part 2 - Putting values in a circular buffer**

At this point you have already made basic processing of data which should work well when implemented.  
However, this comes at the cost of having to do multiple sensor readings per processed measurement.  
This is sometimes not optimal for a setup.  
A solution to this is, instead of taking a whole new set of sensor readings, you only take one new sensor reading and replace it with the oldest sensor reading.  
This is also known as a boxcar filter or a simple moving average (SMA).   You can read more about them here: [https://en.wikipedia.org/wiki/Moving_average](https://en.wikipedia.org/wiki/Moving_average) 

In order to swap out a new sensor reading with the oldest sensor reading you can use a circular buffer. You can use the `collections` library to create a circular buffer.  
The circular buffer behaves almost the same way as a list so your implementation of the dynamic mean function should also work with the circular buffer.

1. In `SignalProcessing.py`, import the `collections` library.
2. Create a buffer with the size of 5 using the following code: `buffer = collections.deque(maxlen=5)`.
3. Add 5 new values to the buffer using `getNoisy` with the inputs of `getNoisy(4, 1)`. To add values use the function: `buffer.append(value)`.
4. Print the buffer.
5. Add a new value to the buffer using `getNoisy` with the inputs of `getNoisy(4, 1)`.
6. Print the buffer and observe the size is still and a value has been overridden.

You should now have a working circular buffer for you sensor readings.

**Part 3 - Combining the parts and testing the solution**

At this point you should have three different parts ready.  
The first being the function generating fictive noisy sensor readings.  
The second being the function which calculates the mean of a list containing floats.  
The third being the circular buffer for holding sensor readings.   

Combine the parts in the following way to set up a test of the filtering process:

1. Fill the buffer using `getNoisy` with the inputs of `getNoisy(4, 1)`.
2. Create a list for collecting the processed measurements called `res`.
3. Create a `for` loop running 20 times.
4. In the `for` loop, add a new sensor reading to the buffer using `getNoisy` with the inputs of `getNoisy(4, 1)`.
5. In the `for` loop, calculate the mean of the values in the buffer using `getMean` and add the result to the list `res`.
6. After the `for` loop, print the values of the list `res`.
7. Create a new list called `resErrs` containing the absolute values of `res` after having subtracted 4 from the values of `res`. eg. `resErrs[0] = abs(res[0] - 4)`. This list contains the size of the error for each result.
8. Use `getMean` with the list `resErrs` to calculate the mean error and print it.

You should be able to use this approach in your project by exchanging the `getNoisy` input for the buffer with an actual sensor reading.
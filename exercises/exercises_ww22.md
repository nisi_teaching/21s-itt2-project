---
Week: 22
tags:
- Report
- Hand-in
---

# Exercises for ww22

## Exercise 1 - Project wrap-up

### Information

This exercise will help your team and you wrap-up the Project course.

### Exercise instructions

**Part 1 - Spring cleaning**
Take the time to clean up your GitLab project(s). Make sure all references are present and that they are accessible.

**Part 2 - Easy fixes and features**
Spend some time implementing easy fixes / features. The goal should be to have the solution in a working state for later referencing.

**Part 3 - Touch-up documentation**
Go through your current documentation and make sure it is up to date. Spend some time specifically on your diagrams. You can test your diagrams by having people outside of the team look at them and then ask them to explain it to you.

**Part 4 - Missing exercises**
Practice is important. If there are any exercises that you have skipped, now is your chance to get them done.

## Exercise 2 - Report hand-in

### Information

To practice writing reports you need to write a report with a page count of 8-12 standard pages.

A standard page is 2400 characters including spaces and footnotes.

The title page, list of contents, bibliography and annexes do not count towards the required number of pages. Annexes are not assessed.

### Exercise instructions

The hand-in is a pdf handed in on wiseflow. It needs to contain the following:

* Frontpage with team member names, semester, course, school logo, date and character count
* Numbered table of contents with page numbers
* A description of your project.
* Explain how you structered team work in accordance to scrum (meetings, workload, collaboration, issue management, milestones etc.)
* Use case - what problem is the system trying to solve ?
* Block diagram
* Documentation of system tests
* Link to project gitlab page (a webpage with content specified in week 19)
* Suggestions on how to proceed development to improve or expand the system (ie. what parts didn't work and what are the challenges?)

Large diagrams etc. needs to be put in the appendix. 
Note that this is a team hand-in.

Please link to code on Gitlab instead of including large code blocks, explain them with flow charts and maybe snippets of essential code (1-3 lines) if needed.

### Follow up

After hand-in, the lecturers will review the handed in documents.
Results are communicated through wiseflow.
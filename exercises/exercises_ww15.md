---
Week: 15
tags:
- Documentation
- Node-Red
- Internationalization
---

# Exercises for ww15

## Exercise 1 - Node red internationale workshop (hosted by Fontys University)

### Information

Together with students from the Netherlands and Belgium we are going to attend a workshop about Node red, MQTT and maybe a bit of javascript.

The entire workshop is hosted by Lecturers Renáta and Karin from Fontys university in Eindhoven.

### Exercise instructions

Log in via Microsoft teams and participate in the workshop.

## Exercise 2 - Documentation up to date

### Information

This is a team exercise.

This exercise will help you make sure your documentation is up to date. You already followed up on some of the documentation in week 11. This exercise will give you the opportunity to do it again and make sure new documentation is included as well.

### Exercise instructions

**Part 1 - Clean up your issue board**

Make sure your issue board is up to date and ready to use.

**Part 2 - Make sure all documentation is present in your `readme.md` and up to date**

Here is a recap of the documentation the `readme.md` should link to from the exercise in week 11:

1. Project plan
2. Pre mortem
3. System brainstorm
4. System overview (block diagram)
5. Use cases
6. Requirement draft
7. MQTT introduction solution code (week 6, exercise 2)
8. OME education research
9. Measuring techniques research (week 7, exercise 1)
10. Sensor requirement draft
11. List of possible sensors (week 7, exercise 3)
12. Sensor data via. RPi and MQTT solution code (week 7, exercise 4)
13. Scrum master, facilitator, secretary on rotation (remember to include a plan)
14. Milestone
15. Data persistence using MongoDB exercise recreation steps and troubleshooting tips (week 9, exercise 1)
16. Proof of concept video

At this point your `readme.md` should also link to the following documentation:

17. Projects created in the teams subgroup

**Part 3 - Catching up**

Get an overview of the things that you are behind with. This includes exercises meant to be done as the team but also exercises meant to be done individually. Plan out how and when you are going to work on these.  
---
Week: 18
tags:
- CI
- Docker
- Shell script
- Make
- Gitlab pipelines
- Business understanding
- Business
- Company types
- Organisations
- Organisational chart
---

# Exercises for ww18

## Exercise 1 - Custom RPi images

### Information

IoT sensor systems are fragile when they are deployed and if anything happens to them you need to be able to quickly replace them at the customer site.

Having a RPi image with all your needed scripts (MQTT client etc.) and dependencies (python etc.) enables you to get a new sensor system up and running in short time. 

This exercise gives you instructions on setting up automatic buld of RPi images using gitlab's CI/CD pipeline.

This exercise is a bit advanced and there will be a walkthrough with examples Monday morning in week 18.

### Exercise instructions

1. Make a list of needed scritps and installed dependencies needed on your sensor system.

2. Fork the [raspberry pi static image project](https://gitlab.com/moozer/raspberry-image-static) to your team's gitlab group.  

    Fork instructions here [https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)

3. Copy files to the forked project

    Make a directory in the forked repository and upload your python scripts and other needed files.  
    

4. Edit script to copy files on build

    Examine and edit `scripts/copy_files.sh` to copy your scripts during the build, from the directory created in step 3, to your desired location on the generated image 

    The best practice is to git clone from your gitlab projects, removing the need for step 3, but it might be too complicated to begin with. 

5. Examine and edit `update_image.sh` if needed.  
  
    This is where you install dependencies to the generated image and set permissions on folder/files you added in step 6.

6. More information is in the projects [documentation](https://gitlab.com/moozer/raspberry-image-static/-/blob/master/readme.md)

    That includes setting up networking and building the image both locally and using gitlab.  
    It is recommended to use gitlab to build the image, especially if you are on windows.  
    Linux should be more manageable.

7. Generate the image.

    Either do this from the command line or have gitlab do it for you.  
    On completion of the build, the image is published to the forked project gitlab pages.  
    The url can be found in the forked project's settings under pages.

8. Copy the image to an SD card ([Balena etcher](https://www.balena.io/etcher/) is a great application for this) and test that the image work as intended on your RPi.


## Exercise 2 - Business understanding

### Information

This is a team exercise. In this exercise you will look into how a business, creating the solutions similar to the ones you are making for this project, works. You will also try to set up your own fictional company, with the goal of selling the product you developed during the project or producing similar solutions for other customers or perhaps both.  

Create a document on gitlab for this exercise and link to it from your `readme.md`.

### Exercise instructions

**Part 1 - Similar companies**

Start by researching companies that sell similar products / services. You should take note of the following:
1. Size of the company.
2. Their target customer - B2B or B2C?, What industry area is targeted?, etc.
3. How do they make money - Selling a product?, Selling a service?, etc.

**Part 2 - Company types in Denmark**

Research the following company types present in Denmark:
1. IVS – Entrepreneur company
2. ApS – Private Limited Company
3. A/S – Public Limited Company

You can read about the company types on the following page: [https://www.companyformations.ie/company-formations/denmark-company-formation/](https://www.companyformations.ie/company-formations/denmark-company-formation/)  

Choose a company type and write down the arguments for choosing the company type for your fictional company.

**Part 3 - Organisational chart**

An easy way of getting an overview of a larger organisation is through an organisational chart. As an example, take a look at the organisational chart for UCL found at: [https://www.ucl.dk/globalassets/07.-om-os/organisationsdiagammer--dkeng/ucl_org_int_navne_jan20213.jpg](https://www.ucl.dk/globalassets/07.-om-os/organisationsdiagammer--dkeng/ucl_org_int_navne_jan20213.jpg)  

Draw up an organisational chart for your fictional company.

## Exercise 3 (Bonus) - MQTT TLS

### Information
This exercise will show you how to set up TLS for the Mosquitto MQTT broker you are using. Setting up and using TLS secures the trafic between the clients and the broker. This can be done as a team exercise or by one of the team members.

It is a good idea to back up your current project before adding TLS or creating a TLS version as a seperateted project.

### Exercise instructions

**Part 1 - Setting up the broker**

1. SSH into the VM running the mosquitto broker
2. Stop the broker using `sudo services mosquitto stop`
3. Navigate to /etc/mosquitto/certs using `cd /etc/mosquitto/certs`
4. Run the following commands to set up TLS certificates:
5. `sudo openssl genrsa -des3 -out ca.key 2048`
6. `sudo openssl req -new -x509 -days 1826 -key ca.key -out ca.crt`
7. `sudo openssl genrsa -out server.key 2048`
8. `sudo openssl req -new -out server.csr -key server.key`
9. `sudo openssl x509 -req -in server.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out server.crt -days 360`
10. Navigate to /etc/mosquitto/conf.d using `cd cd ../conf.d`
11. Create a new settings file using `sudo nano settings.conf`
12. In nano write the following in the new settings file:
port 8883
cafile /etc/mosquitto/certs/ca.crt
certfile /etc/mosquitto/certs/server.crt
keyfile /etc/mosquitto/certs/server.key
13. Start the broker using `sudo services mosquitto start`
14. On azure, create a new inbound rule for port 8883

**Part 2 - On the client**
1. From the broker, download the ca.crt file located in the /etc/mosquitto/certs folder. If you are running linux you can use the scp command, eg. `sudo scp <username>@<broker VM IP>:/etc/mosquitto/certs/ca.crt <local destination folder>`
2. Modify the client program to use port 8883
3. Add the TLS certificate path to the client object by adding `client.tls_set(<path to ca.crt>)` to the program
4. Set the client to use insecure TLS by adding `client.tls_insecure_set(True)` to the program

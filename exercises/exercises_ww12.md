---
Week: 12
tags:
- node-red
- function block
- Javascript
- JS
- Sensor purchase
---

# Exercises for ww12

## Exercise 1 - Requirements agreed and decided

### Information

The requirements you have for the minimum viable product milestone should be agreed and decided with to your product owners. Besides being in an agreement this is to make sure that your MVP will add value to the project.

### Exercise instructions

1. On your meeting with the OME's present the plans and requirements you have for the MVP
2. Make adjustments according to the feedback you get from the OME's
3. Update Milestone description and product backlog

## Exercise 2 - Node-red `function` node and Javascript

### Information
This exercise will introduce you to the `function` node in node-red. In order to usage this node you will need to have a basic understanding of the Javascript language. The `function` node allows you to build customized nodes and are highly flexible in their usage.

### Exercise instructions  

**Part 1 - Looking into Javascript**

Before starting with the `function` node you will need a basic understanding of Javascript. Since both Javascript and Python are scripting languages, they function very similarly. The major differences is in their syntax and the area they are typically used. Take a look at the following chapters from W3Schools Javascript tutorial and try to answer the questions at the bottom.

1. JS Syntax: [https://www.w3schools.com/js/js_syntax.asp](https://www.w3schools.com/js/js_syntax.asp)
2. JS Variables: [https://www.w3schools.com/js/js_variables.asp](https://www.w3schools.com/js/js_variables.asp)
3. JS Operators: [https://www.w3schools.com/js/js_operators.asp](https://www.w3schools.com/js/js_operators.asp)
4. JS Datatypes: [https://www.w3schools.com/js/js_datatypes.asp](https://www.w3schools.com/js/js_datatypes.asp)
5. JS Functions: [https://www.w3schools.com/js/js_functions.asp](https://www.w3schools.com/js/js_functions.asp)
6. JS Objects: [https://www.w3schools.com/js/js_objects.asp](https://www.w3schools.com/js/js_objects.asp)
7. JS Conditions: [https://www.w3schools.com/js/js_if_else.asp](https://www.w3schools.com/js/js_if_else.asp)
8. JS Loop: [https://www.w3schools.com/js/js_loop_for.asp](https://www.w3schools.com/js/js_loop_for.asp)
9. JS Type conversion: [https://www.w3schools.com/js/js_type_conversion.asp](https://www.w3schools.com/js/js_type_conversion.asp)

When initializing a JS object it look similar to initializing a specific python datatype, what datatype is this?  
When creating a JSON string in python, what datatype should be given as an input?  

**Part 2 - Looking into the `function` node in node-red**

Node-red has the following guide on how to use the `function` node: [https://nodered.org/docs/user-guide/writing-functions](https://nodered.org/docs/user-guide/writing-functions)  

Take a look at the following chapters:

1. Writing a Function
2. Multiple Outputs
3. Multiple Messages
4. Logging events
5. Storing data

**Part 3 - Implement a `function` node**

Implement a `function` node that takes in a msg from mqtt and outputs a msg to a `chart` node. The `function` node should output a msg to the `chart` node with information that needs to be displayed from an incoming mqtt msg.

The output msg should follow the format needed by the `chart` node. The format you going to need in this case is most likely the following: {topic: "your topic", payload: \<your data>, timestamp: \<your timestamp>}

Ex. {topic:"temperature", payload:22, timestamp:1520527095000}

You can read more about the formats of the `chart` node here: [https://github.com/node-red/node-red-dashboard/blob/master/Charts.md](https://github.com/node-red/node-red-dashboard/blob/master/Charts.md)

The implemented `function` node should be able to replace the `change` node used in week 10 exercise part 4.

## Exercise 3 - Sensor purchase 

### Information

In order to get the required sensors for your MVP you need to order them this week.
The way you order is by writing them in a list. Lectures will take care of actual ordering, this is because the store accounts are tied to UCL.

Remember that your purchase limit is 500 dkk. for the entire team.
You are only allowed to order from these suppliers:

* RS components [https://dk.rs-online.com/web/](https://dk.rs-online.com/web/)
* Farnell [https://dk.farnell.com/](https://dk.farnell.com/)

### Exercise instructions

1. From your suitable sensors list produced in week 07 decide what you will order
2. Make sure that the sensor(s) are in stock at the above mentiond suppliers
3. Edit your teams issue description at [https://gitlab.com/21s-itt2-datacenter-students-group/examples/sensor-orders/-/boards](https://gitlab.com/21s-itt2-datacenter-students-group/examples/sensor-orders/-/boards)
4. Assign yourself to the issue
5. When you are done editing the order notify the lectures with by tagging us in a comment to the issue, our gitlab usernames are: `@npes` `@GravityVoid`
6. Deadline for ordering is **Wednesday March 24 17:00**

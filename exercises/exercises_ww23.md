---
Week: 23
tags:
- Project site
- Project
- Presentability
- Website
- Finishing touches
---

# Exercises for ww23

## Exercise 1 - Gitlab project finishing touches

### Information

This exercise will make sure you have a presentable project for future use, eg. when searching for an internship.  
The goal is to make it presentable for employees and others witout prior knowledge of the project.

### Exercise instructions

1. Make sure you have completed exercise 1 from week 19 (Creating a project website).
2. Check the website contains the content listed in the exercise.
3. Make sure all the code and documents are up to date on GitLab.
4. Re-organise your GitLab so it will be easier for you to navigate at a later point.
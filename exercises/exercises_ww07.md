---
Week: 07
tags:
- sensor purchase
- research
- raspberry pi
- mqtt
- measuring techniques
---

# Exercises for ww07

## Exercise 1 - How to measure current/cooling/ventilation

### Information
In this exercise you will have to look into different measuring techniques related to the topics of current, temperature and airflow.

### Exercise instructions
You should take notes on the general idea behind the techniques under examination. You should also note down any potential downsides/upsides of the techniques, eg. generally low accuracy, susceptible to dust, etc.

**Part 1 - Preparations**
1. Create a document for taking notes
2. Include the document in your gitlab project and link to the document from your `README.md` 

**Part 2 - Current**  

3. Examine Hall sensors

The following resources can be used:  
* [https://dk.farnell.com/sensor-current-sensor-technology](https://dk.farnell.com/sensor-current-sensor-technology)  
* [https://se.rs-online.com/web/generalDisplay.html?id=ideas-and-advice/hall-effect-sensors-guide](https://se.rs-online.com/web/generalDisplay.html?id=ideas-and-advice/hall-effect-sensors-guide)  
* [https://www.youtube.com/watch?v=RrQA_-YETJw&ab_channel=SensorElectronicTechnology](https://www.youtube.com/watch?v=RrQA_-YETJw&ab_channel=SensorElectronicTechnology)  
* [https://www.youtube.com/watch?v=jdgU49ne4gA&ab_channel=LudicScience](https://www.youtube.com/watch?v=jdgU49ne4gA&ab_channel=LudicScience)  

**Part 3 - Temperature**

4. Examine Thermocouples 
5. Examine Resistance Temperature Detectors (RTDs)
6. Examine Thermistors

The following resources can be used for thermocouples:  
* [https://www.thermocoupleinfo.com/](https://www.thermocoupleinfo.com/)  
* [https://www.youtube.com/watch?v=abC-7OIwgTU&ab_channel=RSPSupply](https://www.youtube.com/watch?v=abC-7OIwgTU&ab_channel=RSPSupply)  

The following resources can be used for RTDs:
* [https://www.jms-se.com/rtd.php](https://www.jms-se.com/rtd.php)  
* [https://www.youtube.com/watch?v=nhFECmPuUCY&ab_channel=RSPSupply](https://www.youtube.com/watch?v=nhFECmPuUCY&ab_channel=RSPSupply)  
* [https://www.youtube.com/watch?v=WNs8FBl3c7M&ab_channel=MadgeTech%2CInc.](https://www.youtube.com/watch?v=WNs8FBl3c7M&ab_channel=MadgeTech%2CInc.)  

The following resources can be used for thermistors:  
* [https://www.omega.com/en-us/resources/thermistor](https://www.omega.com/en-us/resources/thermistor)  
* [https://www.youtube.com/watch?v=DDdlzDFnFu8&ab_channel=RSPSupply](https://www.youtube.com/watch?v=DDdlzDFnFu8&ab_channel=RSPSupply)  
* [https://www.youtube.com/watch?v=D2SWOVWMdU8&ab_channel=Digi-Key](https://www.youtube.com/watch?v=D2SWOVWMdU8&ab_channel=Digi-Key)  

The following resources can be used for comparing the types:  
* [https://www.youtube.com/watch?v=YugXyuTfXtk&ab_channel=OMEGAEngineering](https://www.youtube.com/watch?v=YugXyuTfXtk&ab_channel=OMEGAEngineering)  
* [https://www.omega.com/en-us/resources/rtd-vs-thermistors](https://www.omega.com/en-us/resources/rtd-vs-thermistors)  

**Part 4 - Humidity**

7. Examine capacitive humidity sensors
8. Examine resistive humidity sensors
9. Examine thermal conductivity humidity sensors

The following resources can be used:
* [https://www.electronicshub.org/humidity-sensor-types-working-principle/](https://www.electronicshub.org/humidity-sensor-types-working-principle/)  
* [https://www.electronicsforu.com/tech-zone/electronics-components/humidity-sensor-basic-usage-parameter](https://www.electronicsforu.com/tech-zone/electronics-components/humidity-sensor-basic-usage-parameter)  

**Part 5 - Airflow**

10. Examine hot wire airflow sensors
11. Examine differential pressure sensors

The following resources can be used:  
* [https://sensing.honeywell.com/honeywell-sensing-pressure-or-airflow-sensors-technical-note-008329.pdf](https://sensing.honeywell.com/honeywell-sensing-pressure-or-airflow-sensors-technical-note-008329.pdf)  
* [https://www.coulton.com/beginners_guide_to_differential_pressure_transmitters.html](https://www.coulton.com/beginners_guide_to_differential_pressure_transmitters.html)  
* [https://www.youtube.com/watch?v=KfBhVttKN4o&ab_channel=TheAutomotivesGlobal](https://www.youtube.com/watch?v=KfBhVttKN4o&ab_channel=TheAutomotivesGlobal)  
* [https://www.youtube.com/watch?v=dCjIqGGiVdw&ab_channel=%E3%82%AA%E3%83%A0%E3%83%AD%E3%83%B3%2FOMRON](https://www.youtube.com/watch?v=dCjIqGGiVdw&ab_channel=%E3%82%AA%E3%83%A0%E3%83%AD%E3%83%B3%2FOMRON)

## Exercise 2 - Sensor requirements

### Information
Start looking into the potential requirements that the sensors in a datacenter could have.

### Exercise instructions
1. Create a list of potential requirement areas for sensors in a datacenter. eg. temperature, humidity, cost, etc.
2. Structure the list in terms of the importance of the requirements
3. Make educated guesses for the values of the requirement
4. Include the list in your gitlab project and link to the list from your `README.md` 


## Exercise 3 - Research available suitable sensors

### Information
Sourcing hardware components is time-consuming because a lot of components exists and you have to evaluate them from a variety of parameters. 

From exercise 1 you now know different techniques to measure different things in the datacenter.  
From exercise 2 you have knowledge on how to evaluate different sensors and their properties.  
The purpose of this exercise is to research available sensors from selected suppliers and build a catalog of sensors that potentially is going to be a part of your system. 

It is important that you find as many sensors as possible, to have something to choose between, you should be able to find information on at least 10 different sensors.

You are limited to using 2 different suppliers and your budget for the entire team is 300-500 dkk.
It is your choice if you want to buy sensors for each member of the team or only a couple of sensors that can be shared between team members, as long as you stay within the budget!  

*Note that this exercise does not include ordering of components, you need more information from the OME's about the project, before you are ready for ordering*

### Exercise instructions

1. From exercise 1 and 2 create a sensor requirement list. The list needs to include alt the different measuring techniques and sensor precision. 
2. Get familiar with navigating the available suppliers, try out the filters, catagories etc.:
  * [https://dk.rs-online.com/web/](https://dk.rs-online.com/web/)  
  * [https://dk.farnell.com/](https://dk.farnell.com/) 
3. Create a file in .md format, in the file create a [table](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#tables) and fill it with available sensors that fits your criterias.  
As a minimum the list needs to include the parameters shown below (DHT22 used as example)

|Measuring technique|Sensor name|Link to supplier|Cost|availability|Pros|Cons|
|:---|:---|:---|:---|:---|:---|:---|
|Capacitive humidity sensing digital temperature | DHT22 | [https://dk.farnell.com/dfrobot/sen0137/dht22-temp-humidity-sensor-arduino/dp/3517874?st=dht-22](https://dk.farnell.com/dfrobot/sen0137/dht22-temp-humidity-sensor-arduino/dp/3517874?st=dht-22) | 39,19 dkk | in stock (13 pcs) | low cost, easy integration | Large size, low update freq. (2 sec.) |  

4. Include the list in your gitlab project and link to the list from your `README.md` 

## Exercise 4 - Sensor data via RPi and MQTT 

### Information

In week 6 you tried out MQTT and created a program that can send and receive MQTT messages.
The purpose of this exercise is to integrate a sensor of choice into the code and send real sensor data via a MQTT broker.

A boiler plate MQTT example for inspiration is here: [https://gitlab.com/npes-py-experiments/mqtt-tests](https://gitlab.com/npes-py-experiments/mqtt-tests)

### Exercise instructions

1. Choose a sensor from ie. the freenove kit
2. Connect it to your raspberry pi and test that it works (write a small test program)
3. Include the sensor test program in your MQTT program from week 6 and test that it can send and recieve sensor data.
4. Decide in you team on a topic structure that can include sensor data from all team members
5. Modify your program to be able to receive MQTT sensor messages from all of your team members
6. In your team decide on relevant tests for the system (ie. a list of tests your will do and criterias for passing the tests)
6. In the team test according to test plan.
7. Remember to reguarly commit your code and test plan, in your gitlab project and link to the files from the `README.md` file
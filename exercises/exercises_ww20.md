---
Week: 20
tags:
- Unit test
- Testing
- CI/CD
- GitLab CI/CD
---

# Exercises for ww20

## Exercise 1 - Unit testing

### Information

This is a solo exercise, feel free to do pair programming.

In this exercise you will look into creating a unit tests for your code and using GitLab to trigger the tests when pushing code to a repository. Unit testing is a good tool against function breaking updates and is widely used in the IT industry. Alternatively, unit testing can also help you design solutions, as it makes you focus on **what** a unit does rather than **how** it does it.

### Exercise instructions

**Part 0 - Creating some functionality which needs testing**

1. Create a python file called `functions.py`.
2. In `functions.py`, add the function called add which is seen below.

```python
def add (number1, number2):
    return number1 + number2
```

**Part 1 - Creating the tests**

In order to create the tests we are going to use the built in library called `unittest`.
We start by creating the framework for the tests.

1. Create a python file called `test.py`.
2. In `test.py`, import `functions.py` and `unittest`.
3. In `test.py`, add a new class called `TestAdd` and make it inherit from `unittest.TestCase`.

Next we would like to create a test, testing some given input to the `add` function against an expected output of the function. In order to do this we will use the `assertEqual` function inherited from the `unittest.TestCase` superclass. `assertEqual` takes in two values and pass if they are equal. You can call `assertEqual` in a function like this: `self.assertEqual(val1, val2)`.

4. In `TestAdd`, add a function `test_equal`.
5. In `test_equal`, call `assertEqual` with the first input set to be the result of `add(2, 2)` and the second input to 4.

Only testing a function against the expected result is not always enough as the function might always pass even in cases where it should fail. It might also not be possible to implement an equality test. In this exercise we would like to create a test, testing some given input to the `add` function against an incorrect result.

6. In `TestAdd`, add a function `test_false`.
7. In `test_false`, call `assertNotEqual` with the first input set to be the result of `add(2, 2)` and the second input to 8.

It is also common to test the type returned by the function, especially in a dynamically typed language like python. To test the type of the result against the expected type, we can use `assertEqual` like this: `self.assertEqual(typeof(res), type)`.

8. In `TestAdd`, add a function `test_type`.
9. In `test_type`, call `assertEqual` with the first input set to be the type of result of `add(2, 2)` and the second input to `int`.

Now that the test are set up, we would like them to be executed when `test.py` is run. To do this simply add the following at the end of `test.py`.

```python
if __name__ == '__main__':
    unittest.main()
```

10. Make the test execute when `test.py` is run.

**Part 2 - Setting up the GitLab pipeline**

1. Create a new GitLab project for the exercise
2. Add a new file in the GitLab project called `.gitlab-ci.yml` (This is YAML file).
3. In `.gitlab-ci.yml`, add the following code:

```yml
stages: # Stages in the pipeline
 - test # Stage name

test: # Stage
 stage: test # Stage name
 image: python # Image for the stage executer
 script: # Command(s) to be executed (linux commands since we are using a linux image)
  - python3 test.py # The test script we just made
```

4. Add, commit and push `functions.py` and `test.py`.

The pipeline should trigger on GitLab and hopefully pass all of your tests. You can see the pipeline process under CI/CD in the left sidebar on the GitLab project webpage.

**(Bonus) Part 3 - Expanding**

Create a function of you own design and test it using the knowledge gained in the previous parts. You can also try to test a function you have previously created (like your mqtt client code etc.) or built-in funtionality in python.

## Exercise 2 - System test plan

### Information

This is a team exercise.

Your system has multiple components which can all fail in different ways.  
Maybe you already experienced failures while developing or you have thought of reasons for failing system parts.  
Instead of waiting for your system to fail you need a test plan that you can use reguarly to ensure that your system is functional.  
A test plan can also be used in conjunction with planned maintenance to ensure that the system works as intended after maintenance.   

### Exercise instructions

1. From your block diagram, create a list with possible types of failures for each block/component
2. Expand the list with ways to test for each type of failure. This can be manual tests, automated tests (gitlab ci/cd, unit test etc.) or simply a checklist of things to check to ensure a healthy operational system.
3. Formalize the list into a step by step checklist on how to test the system. 
4. Identify what needs to be done, this might be setting up unit tests etc. and distribute work within the team
5. Try out the test plan from a system cold start, correct the plan if necessary.
6. Include the test plan on your gitlab page. 
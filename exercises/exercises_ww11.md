---
Week: 11
tags:
- Gitlab
- Minimum viable product
- MVP
---

# Exercises for ww11

## Exercise 1 - Catch/clean up

### Information
This exercise will have look back and give you a chance to catch up on some of the exercises you might have missed and cleanup your Gitlab. The first part of the exercise should be done together by the team.

### Exercise instructions

**Part 1 - Clean up your issue board**

Make sure your issue board is up to date and ready to use.

**Part 2 - Make sure all documentation is present in your `readme.md` and up to date**

At this point your `readme.md` should link to the following documentation:
1. Project plan
2. Pre mortem
3. System brainstorm
4. System overview (block diagram)
5. Use cases
6. Requirement draft
7. MQTT introduction solution code (week 6, exercise 2)
8. OME education research
9. Measuring techniques research (week 7, exercise 1)
10. Sensor requirement draft
11. List of possible sensors (week 7, exercise 3)
12. Sensor data via. RPi and MQTT solution code (week 7, exercise 4)
13. Scrum master, facilitator, secretary on rotation (remember to include a plan)
14. Milestone
15. Data persistence using MongoDB exercise recreation steps and troubleshooting tips (week 9, exercise 1)
16. Proof of concept video

**Part 3 - Catching up**

Get an overview of the things that you are behind with. This includes exercises meant to be done as the team but also exercises meant to be done individually. Plan out how and when you are going to work on these.  

As a note, exercises that are meant to be done individually can be together. The important point is that everyone in team takes part in solving this type of exercise. It could be a good idea to use pair programming for this type of exercise. This will allow you to learn from each other and will give you fewer and better solutions for the exercise.


## Exercise 2 - Minimum Viable Product

### Information

In week 15 you need to deliver a minimum viable product (MVP) version of your system. But what is a minimum viable product and how will you get there in the next 5 weeks?

### Exercise instructions

1. Research the meaning of the term minimum viable product. Suggestions for research are:
  * [https://en.wikipedia.org/wiki/Minimum_viable_product](https://en.wikipedia.org/wiki/Minimum_viable_product)
  * [https://www.productplan.com/glossary/minimum-viable-product/](https://www.productplan.com/glossary/minimum-viable-product/)
  * [https://www.agilealliance.org/glossary/mvp](https://www.agilealliance.org/glossary/mvp)
  * [https://youtu.be/E4ex0fejo8w](https://youtu.be/E4ex0fejo8w)
  * [https://youtu.be/1FoCbbbcYT8](https://youtu.be/1FoCbbbcYT8)

2. In your team discuss what you can use the minimum viable product for (ie. how will you test it on your customers, the OME's)
3. Decide in your team what features will be included in your minimum viable product
4. Write your features and purpose of the minimum viable product in a milestone on gitlab (the goal to reach!)
5. Update your product backlog with task related to the minimum viable product milestone (how will you reach the goal?)
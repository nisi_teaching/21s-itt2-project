---
Week: 15
Content: Minimum viable product
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 15

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Participate in international workshop


### Learning goals

* None this week


## Deliverables

* issue board is up to date and ready to use

## Schedule

### Monday

This day we will use microsoft teams to attend the workshop, a link has been sent to your UCL school email. Email subject is "International Node-RED workshop".

| Time | Activity |
| :---: | :--- |
| 9:00 | Node red workshop hosted by Fontys university |
| 16:15 | End of day |

### Wednesday

Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | OME meetings |
| 13:00 | Lunch (agree with the OME's) | 
| 15:00 | Hands-on time |
| 16:15 | End of day |

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - Node red internationale workshop (hosted by Fontys University)

...

### Exercise 2 - Documentation up to date

...

## Comments

* None at this time

---
Week: 07
Content: Proof of concept
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 07

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Complete exercises
* Participate in scrum activities
* Participate in Q&A session

### Learning goals

* Research sensor technologies
  * Level 1: The student knows of multiple sensor technologies for measuring physical properties
  * Level 2: The student know of benefits and drawbacks of different sensor technologies
  * Level 3: The student can select sensor technologies based on a set of requirements

* Research available suitable sensors
  * Level 1: The student knows how to search suppliers
  * Level 2: The student can set up hardware requirements 
  * Level 3: The student can methodically create an overview of hardware components 

* Sensor data via RPi MQTT
  * Level 1: The student can read sensor data using RPi and python 
  * Level 2: The student can integrate sensor data and MQTT in a program
  * Level 3: The student create a test plan, test according to it and documentresults.

## Deliverables
* Document on sensor technology research
* List of potential requirements
* List of available, suitable sensors
* Working sensor/MQTT code
* Completed test plan

## Schedule

Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | Q&A Session |
| 13:00 | Lunch | 
| 13:45 | Hands-on time |
| 16:15 | End of day |

### Wednesday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | Q&A Session |
| 13:00 | Lunch | 
| 13:45 | Hands-on time |
| 16:15 | End of day |

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - How to measure current/cooling/ventilation with sensors 

...

### Exercise 2 - Required sensor precision

...

### Exercise 3 - Research available suitable sensors 

...

### Exercise 4 - Sensor data via RPi and MQTT 

...

## Comments

* None at this moment

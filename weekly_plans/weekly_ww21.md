---
Week: 21
Content: Project presentations
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 21

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Presentation planned and ready


### Learning goals

Communicate orally and in writing with both professionals and customers
* Communicate key results

* Presentations - the student is able to:  
  * Level 1: Plan a project presentation
  * Level 2: Communicate key results
  * Level 3: Participate in academic and interdisciplinary collaboration and project work in connection with technology development


## Deliverables
* Team project presentation 

## Schedule

Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

### Monday

No lectures because of Whit Monday

### Wednesday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 11:45 | Lunch |
| 12:30 | Presentations with OME - timetable in exercise 2|
| 15:50 | End of day |

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - Presentation prep

...

### Exercise 2 - Presentations

...

## Comments

* None at this time

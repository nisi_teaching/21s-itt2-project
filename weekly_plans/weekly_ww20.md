---
Week: 20
Content: Consolidation
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 20

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* ...


### Learning goals

* Unit testing
  * Level 1: The student can implement a unit test using python
  * Level 2: The student can trigger a unit test through the GitLab CI/CD pipeline
  * Level 3: The student is able to design a unit test

* System test plan
  * Level 1: The student knows what a test plan is
  * Level 2: The student can perform a simple test
  * Level 3: The student can design and implement a system test plan

## Deliverables
* ...

## Schedule

Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | Q&A Session |
| 13:00 | Lunch | 
| 13:45 | Hands-on time |
| 16:15 | End of day |

### Wednesday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | OME meetings |
| 13:00 | Lunch (agree with the OME's) | 
| 15:00 | Hands-on time |
| 16:15 | End of day |

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - Unit testing

...

### Exercise 2 - System test plan

...

## Comments

* None at this time

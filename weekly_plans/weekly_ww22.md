---
Week: 22
Content: Project finalization
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 22

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* ...


### Learning goals

* Report
  * Level 1: Identify key result to be communicated.
  * Level 2: Plan and structure communication of key results.
  * Level 3: Communicate key results in a written format.


## Deliverables
* ...

## Schedule

Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | Q&A Session |
| 13:00 | Lunch | 
| 13:45 | Hands-on time |
| 16:15 | End of day |

### Wednesday

This day will be without lecturers.

| Time | Activity |
| :---: | :--- |
| 9:00 | Daily scrum - 15 minutes|
| 9:15 | Hands-on time |
| 16:15 | End of day |

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - Project wrap-up

...

### Exercise 2 - Report hand-in

...

## Comments

* None at this time

---
Week: 08
Content: Proof of concept
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 08

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Complete exercises
* Participate in scrum activities
* Participate in OME meeting

### Learning goals

* Scrum master, facilitator, secretary
  * Level 1: The student knows the resposibilities and tasks of a Scrum master, facilitator and secretary
  * Level 2: The student is able to act as a Scrum master, facilitator and secretary
  * Level 3: The team is able to utilize having a Scrum master, facilitator and secretary

* Milestones using gitlab
  * Level 1: The student knows the purpose of milestones
  * Level 2: The student can create milestones using gitlab
  * Level 3: The team can, continuously, manage and work towards a greater goal using milestones

* MQTT broker as a microservice on Azure
  * Level 1: The student knows what microservices are
  * Level 2: The student can set up a virtual machine in the cloud
  * Level 3: The student can install and use a MQTT broker on a cloud virtual machine


## Deliverables
* Working MQTT broker on microsoft azure
* Milestones created on gitlab and issues assigned to milestones
* Plan for the facilitator, scrum master and secretary roles

## Schedule

### Monday

This day is without lecturers, please ask questions on element.

| Time | Activity |
| :---: | :--- |
| 9:00 | Daily scrum - 15 minutes |
| 9:15 | Hands-on time |
| 13:00 | Lunch | 
| 13:45 | Hands-on time |
| 16:15 | End of day |

### Wednesday

Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | OME meetings |
| 13:00 | Lunch (agree with the OME's) | 
| 15:00 | Hands-on time |
| 16:15 | End of day |

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - MQTT broker as a microservice on Azure

...

### Exercise 2 - Scrum master, facilitator, secretary on rotation

...

### Exercise 3 - Milestones using gitlab

...

## Comments

* None at this moment

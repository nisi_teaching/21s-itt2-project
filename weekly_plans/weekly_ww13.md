---
Week: 13
Content: Minimum viable product
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 13

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* None


### Learning goals

* None


## Deliverables
* None

## Schedule

### Monday

No lectures because of easter break

### Wednesday

No lectures because of easter break

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - Easter break 

...

## Comments

* None at this time

---
Week: 17
Content: Consolidation
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 17

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Sensor system failure prevention implemented
* MQTT communication security implemented



### Learning goals  
* System robustness
  * Level 1: The student has knowledge about common IoT sensor failure scenarios
  * Level 2: The student can implement methods to prevent and monitor show failure on both local devices and dashboard
  * Level 3: The student can implement methods to prevent data loss 


* MQTT Security
  * Level 1: Identify security vulnerabilities in mqtt
  * Level 2: Suggest potential solutions for security vulnerabilities in mqLevel 1: tt
  * Level 3: Implement solutions to security vulnerabilities in mqtt



## Deliverables
* ...

## Schedule

All presentations will be online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | Q&A Session |
| 13:00 | Lunch | 
| 13:45 | Hands-on time |
| 16:15 | End of day |

### Wednesday

A class are allowed to work from school physically, rooms are announced on time edit.

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | OME meetings |
| 13:00 | Lunch (agree with the OME's) | 
| 15:00 | Hands-on time |
| 16:15 | End of day |

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - System robustness

...

### Exercise 2 - MQTT security

...

## Comments

* None at this time

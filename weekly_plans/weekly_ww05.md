---
Week: 05
Content:  Project statup
Material: See links in weekly plan
Initials: NISI/MEGR
---

# Week 05

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Scrum
  * A product backlog has been created
  * A sprint backlog has been created
* Pre mortem performed
* Project plan draft completed

### Learning goals

* Scrum
  * Level 1: The student knows the artefacts and meeting types used in scrum
  * Level 2: The student is able to work as part of a team using scrum
  * Level 3: The student is able to manage a team using scrum

* Pre mortem
  * Level 1: The student knows what a pre mortem is 
  * Level 2: The student can perform a pre mortem
  * Level 3: The student can prioritize risks

* Project plan
  * Level 1: The student knows what a project plan is 
  * Level 2: The student knows the importance of a project plan
  * Level 3: The student can create a project plan from a template

## Deliverables
* Gitlab project created
* Links to exercise output included in gitlab project readme.md
* Project plan updated and included in gitlab project
* Pre mortem included in gitlab project


## Schedule

Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 10:15 | Hands-on time |
| 13:00 | Lunch | 
| 13:45 | Hands-on time |
| 16:15 | End of day |

### Wednesday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Daily scrum - 15 minutes|
| 9:45 | Hands-on time |
| 12:30 | Presentations and discussion |
| 13:00 | Lunch | 
| 13:45 | Hands-on time |
| 16:15 | End of day |

## Hands-on time

See the exercise page for details [https://eal-itt.gitlab.io/21s-itt2-project/exercises/](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

### Exercise 1 - Gitlab project setup

...

### Exercise 2 - Start using Scrum

...

### Exercise 3 - The first daily scrum meeting

...

### Exercise 4 - The end of the first sprint loop

...

### Exercise 5 - pre mortem

...

### Exercise 6 - Update project plan

## Comments

* None at this moment

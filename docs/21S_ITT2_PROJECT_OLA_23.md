---
title: '21S ITT2 Project'
subtitle: 'OLA23'
filename: '21S_ITT2_PROJECT_OLA_23'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Mathias Gregersen \<megr@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2021-03-27
email: 'nisi@ucl.dk'
left-header: \today
right-header: OLA23
skip-toc: false
---

# Introduction

In this obligatory learning activity you will work with GIT branches, OLA23 is a team exercise.  
The foundation for completing this OLA is Exercise 2 from Week 14.

Hand-in deadline is communicated through wiseflow.

# Instructions

1. Each team member pick's an issue from the project issue board
2. Each team member create's a branch and pull's it locally (your computer)
3. Work on required changes and make several commits during work (remember to switch branch before commming `git checkout <branch name>`)
4. Pull from master branch to your branch `git pull origin master`
5. Resolve any merge conflicts in your favourite text editor
6. Push to the remote repository `git push origin <branch name>`
7. Create a merge request (remember to link to the issue in the description) and merge it (use gitlabs functionality for this)
8. When every team members has merged their branches, examine the branch graph in your gitlab project. 
Example here: [https://gitlab.com/EAL-ITT/21s-itt2-project/-/network/master](https://gitlab.com/EAL-ITT/21s-itt2-project/-/network/master)
9. Create a combined team step by step guide, including GIT commands and screenshots, to be used when working with branches (new branch, pull branch, checkout branch, pull from master, merge request etc.)

# Hand in

A small team report on how to work with GIT branches.

The report has to include (use this assignment layout as inspiration):

* Frontpage with title, date, authors, contact email address and course name.
* Table of contents with numbered chapters and page numbering
* Page numbering starting from the page after table of contents


1. Screen shot of branch graph (step 8) with explanation of the screenshot
2. Step by step guide (step 9)
3. Benefits of using GIT branches in collaboration with others (your teams reflections in writing)

# Follow up 

After hand-in, the lecturers will review the handed in documents.  

Results are communicated through wiseflow.
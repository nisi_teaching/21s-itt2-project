---
title: '21S ITT2 Project'
subtitle: 'Project hand-in'
filename: 'ITT2_project_handin'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Mathias Gregersen \<megr@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2021-05-17
email: 'nisi@ucl.dk'
left-header: \today
right-header: project hand-in
skip-toc: true
---

# WW22 - Exercise 2 - Report handin

## Information

To practice writing reports you need to write a report with a page count of 8-12 standard pages.

A standard page is 2400 characters including spaces and footnotes.

The title page, list of contents, bibliography and annexes do not count towards the required number of pages. Annexes are not assessed.

## Exercise instructions

The hand-in is a pdf handed in on wiseflow. It needs to contain the following:

* Frontpage with team member names, semester, course, school logo, date and character count
* Numbered table of contents with page numbers
* A description of your project.
* Explain how you structered team work in accordance to scrum (meetings, workload, collaboration, issue management, milestones etc.)
* Use case - what problem is the system trying to solve ?
* Block diagram
* Documentation of system tests
* Link to project gitlab page (a webpage with content specified in week 19)
* Suggestions on how to proceed development to improve or expand the system (ie. what parts didn't work and what are the challenges?)

Large diagrams etc. needs to be put in the appendix.
Note that this is a team hand-in.

Please link to code on Gitlab instead of including large code blocks, explain them with flow charts and maybe snippets of essential code (1-3 lines) if needed.

## Follow up
After hand-in, the lecturers will review the handed in documents.
Results are communicated through wiseflow.